# This is a sample build configuration for Other.
# Check our guides at https://confluence.atlassian.com/x/5Q4SMw for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: python:3.7.2  #atlassian/default-image:2

pipelines:
  branches:
    '**':
        - step:
           deployment: production
           caches:
            - pip

           script:
            - pip install requests
            - apt-get update && apt-get install -y curl jq
            - export BITBUCKET_COMMIT="COMMIT#$BITBUCKET_COMMIT"
            - export BITBUCKET_BRANCH="$BITBUCKET_BRANCH"
            - export BITBUCKET_REPO_OWNER="$BITBUCKET_REPO_OWNER"
            - export BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG"
            - export REPONAME="$REPO_NAME"
            - export AUTHTOKEN="$AUTH_TOKEN"
            - if [ $REPO_NAME != 'master' ]; then
            # Get All Default Reviewer List
            - >
              export DEFAULT_REVIEWERS=$(curl https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/default-reviewers \
              -s -S -f -X GET \
              -H "Authorization: Basic $AUTHTOKEN" | jq '.values' | jq 'map({uuid})' )

            - echo $BITBUCKET_COMMIT
            - echo $BITBUCKET_BRANCH
            - echo $BITBUCKET_REPO_OWNER
            - echo $BITBUCKET_REPO_FULL_NAME
            - echo $AUTH_TOKEN
            - echo $DEFAULT_REVIEWERS
            # Create PR
            - python create-pull-request.py $BITBUCKET_COMMIT $BITBUCKET_BRANCH master $BITBUCKET_REPO_OWNER $REPONAME $AUTHTOKEN $DEFAULT_REVIEWERS
            - fi

  pull-requests:
    '**':
        - step:
            deployment: staging
            caches:
              - pip

            script:
            - pip install requests
            - apt-get update && apt-get install -y curl jq
            - export BITBUCKETCOMMIT="COMMIT#$BITBUCKET_COMMIT"
            - export BITBUCKET_BRANCH="$BITBUCKET_BRANCH"
            - export BITBUCKET_REPO_OWNER="$BITBUCKET_REPO_OWNER"
            - export BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG"
            - export REPONAME="$REPO_NAME"
            - export AUTHTOKEN="$AUTH_TOKEN"
            # Get Pull Request Number
            - >
              export PULL_REQUEST_ID=$(curl https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$REPONAME/commit/$BITBUCKETCOMMIT/pullrequests \ 
              -s -S -f -X GET \ 
              -H "Authorization: Basic $AUTHTOKEN" | jq '.values' | jq 'map({id})' | grep id | cut -d ":" -f2 )
            - echo $PULL_REQUEST_ID
            # Check PR Whether it is approved
            - >
              export IS_APPROVED_TRUE=$(curl https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$REPONAME/pullrequests/$PULL_REQUEST_ID \
              -s -S -f -X GET \
              -H "Authorization: Basic $AUTHTOKEN" | jq '.participants' | jq 'map({approved})' | grep true )
            - echo $IS_APPROVED_TRUE
            # If PR Approved from atleast 1 reviewer, Merge it to master
            - if [ ! -z "$IS_APPROVED_TRUE"]; then
            - python merge-pull-request.py $BITBUCKET_REPO_OWNER $REPONAME $AUTHTOKEN $BITBUCKET_BRANCH master $BITBUCKET_COMMIT
            - fi