#!/bin/sh
import json
import requests
import os
import sys
import ast

def create_pull_request(request_title, source_branch, dest_branch, username, repo_name, authToken, reviewer_list):
    reviewer_list = ast.literal_eval(''.join(map(str, reviewer_list)))
    #reviewer_list = [ {"uuid": str(p['uuid'].replace("{", "").replace("}", "")) } for p in reviewer_list]
    reviewer_list = [ {"uuid": str(p['uuid']) } for p in reviewer_list]
    
    #reviewer_uuid_list = [str(p['uuid'].replace("{", "").replace("}", "")) for p in reviewer_list]
    #reviewer_list = [{'uuid' : reviewer_uuid_list }]

    print('=====request_title===='+request_title)
    print('=====source_branch===='+source_branch)
    print('=====dest_branch===='+dest_branch)
    print('=====username===='+username)
    print('=====repo_name===='+repo_name)
    print('=====authToken===='+authToken)
    print(reviewer_list)

    print('=====createPullRequest====')
    try:
        req = requests.post('https://api.bitbucket.org/2.0/repositories/'+username+'/'+repo_name+'/pullrequests', headers={'Authorization': 'Basic '+authToken}, json= {
            'title': request_title,
            'close_source_branch': 'true',
            'source': {
                'branch': {
                    'name': source_branch
                }
            },
            'destination': {
                'branch': {
                    'name': dest_branch
                }
            },
            'reviewers':  reviewer_list #[{'uuid': 'df37cf49-fa20-46a5-91b9-52952375f81e'}]#reviewer_list
         })

        print('\n Response Status Code===='+str(req.status_code))
        print('\n Response Reason===='+str(req.reason))
    except requests.exceptions.HTTPError as err:
        print('\n Response Status Code===='+str(req.status_code))
        print( err)
        sys.exit(1)
	
if __name__ == "__main__":
    create_pull_request(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7:])